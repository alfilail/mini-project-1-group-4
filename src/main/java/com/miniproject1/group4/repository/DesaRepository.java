package com.miniproject1.group4.repository;

import org.springframework.stereotype.Repository;

import com.miniproject1.group4.entity.DesaEntity;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DesaRepository extends JpaRepository<DesaEntity, String> {

}
