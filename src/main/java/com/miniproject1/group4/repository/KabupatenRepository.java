package com.miniproject1.group4.repository;

import org.springframework.stereotype.Repository;

import com.miniproject1.group4.entity.KabupatenEntity;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface KabupatenRepository extends JpaRepository<KabupatenEntity, String> {
	
}
