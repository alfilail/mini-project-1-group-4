package com.miniproject1.group4.repository;

import com.miniproject1.group4.entity.ProvinsiEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvinsiRepository extends JpaRepository<ProvinsiEntity, String> {
}
