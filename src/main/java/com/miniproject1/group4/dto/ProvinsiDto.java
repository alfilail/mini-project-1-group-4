package com.miniproject1.group4.dto;

public class ProvinsiDto {
	private String kodeprov;
	private String namaprov;
	public String getKodeprov() {
		return kodeprov;
	}
	public void setKodeprov(String kodeprov) {
		this.kodeprov = kodeprov;
	}
	public String getNamaprov() {
		return namaprov;
	}
	public void setNamaprov(String namaprov) {
		this.namaprov = namaprov;
	}
}
