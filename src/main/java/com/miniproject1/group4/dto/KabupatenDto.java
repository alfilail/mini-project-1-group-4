package com.miniproject1.group4.dto;

public class KabupatenDto {
	private String namaKabupaten;
	private String codeKabupaten;

	public String getNamaKabupaten() {
		return namaKabupaten;
	}

	public void setNamaKabupaten(String namaKabupaten) {
		this.namaKabupaten = namaKabupaten;
	}

	public String getCodeKabupaten() {
		return codeKabupaten;
	}

	public void setCodeKabupaten(String codeKabupaten) {
		this.codeKabupaten = codeKabupaten;
	}

}
