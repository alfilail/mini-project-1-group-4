package com.miniproject1.group4.dto;

public class DesaDto {

	private String codeDesa;
	private String namaDesa;
	private String codeKecamatan;
	private String namaKecamatan;
	private String codeKabupaten;
	private String namaKabupaten;
	private String codeProvinsi;
	private String namaProvinsi;

	public String getCodeDesa() {
		return codeDesa;
	}

	public void setCodeDesa(String codeDesa) {
		this.codeDesa = codeDesa;
	}

	public String getNamaDesa() {
		return namaDesa;
	}

	public void setNamaDesa(String namaDesa) {
		this.namaDesa = namaDesa;
	}

	public String getCodeKecamatan() {
		return codeKecamatan;
	}

	public void setCodeKecamatan(String codeKecamatan) {
		this.codeKecamatan = codeKecamatan;
	}

	public String getNamaKecamatan() {
		return namaKecamatan;
	}

	public void setNamaKecamatan(String namaKecamatan) {
		this.namaKecamatan = namaKecamatan;
	}

	public String getCodeKabupaten() {
		return codeKabupaten;
	}

	public void setCodeKabupaten(String codeKabupaten) {
		this.codeKabupaten = codeKabupaten;
	}

	public String getNamaKabupaten() {
		return namaKabupaten;
	}

	public void setNamaKabupaten(String namaKabupaten) {
		this.namaKabupaten = namaKabupaten;
	}

	public String getCodeProvinsi() {
		return codeProvinsi;
	}

	public void setCodeProvinsi(String codeProvinsi) {
		this.codeProvinsi = codeProvinsi;
	}

	public String getNamaProvinsi() {
		return namaProvinsi;
	}

	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}

}
