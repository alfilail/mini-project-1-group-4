package com.miniproject1.group4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.miniproject1.group4.dto.ProvinsiDto;
import com.miniproject1.group4.entity.ProvinsiEntity;
import com.miniproject1.group4.repository.ProvinsiRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/provinsi")
public class ProvinsiController {

	private final ProvinsiRepository provinsiRepository;
	
	@Autowired
	public ProvinsiController(ProvinsiRepository provinsiRepository) {
		this.provinsiRepository = provinsiRepository;
	}
	
	@GetMapping
	public List<ProvinsiDto> get() {
		List<ProvinsiEntity> provinsiEntityList = provinsiRepository.findAll();
		List<ProvinsiDto> provinsiDtoList = provinsiEntityList.stream().map(this::convertToDto)
				.collect(Collectors.toList());
		return provinsiDtoList;
	}
	
	@GetMapping("/{kodeprov}")
	public ProvinsiDto get(@PathVariable String kodeprov) {
		if(provinsiRepository.findById(kodeprov).isPresent()) {
			ProvinsiDto provinsiDto = convertToDto(provinsiRepository.findById(kodeprov).get());
			return provinsiDto;
		}
		return null;
	}
	
	@PostMapping
	public ProvinsiDto insert(@RequestBody ProvinsiDto dto) {
		ProvinsiEntity provinsiEntity = convertToEntity(dto);
		provinsiRepository.save(provinsiEntity);
		return dto;
	}
	
	private ProvinsiEntity convertToEntity(ProvinsiDto dto) {
		ProvinsiEntity provinsiEntity = new ProvinsiEntity();
		provinsiEntity.setKodeProvinsi(dto.getKodeprov());
		provinsiEntity.setNamaProvinsi(dto.getNamaprov());
		return provinsiEntity;
	}
	
	private ProvinsiDto convertToDto(ProvinsiEntity provinsiEntity) {
		ProvinsiDto provinsiDto = new ProvinsiDto();
		provinsiDto.setKodeprov(provinsiEntity.getKodeProvinsi());
		provinsiDto.setNamaprov(provinsiEntity.getNamaProvinsi());
		return provinsiDto;
	}
}

