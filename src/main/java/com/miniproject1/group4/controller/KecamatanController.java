package com.miniproject1.group4.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.miniproject1.group4.dto.KecamatanDto;
import com.miniproject1.group4.entity.KecamatanEntity;
import com.miniproject1.group4.repository.KecamatanRepository;

@RestController
@RequestMapping("/kecamatan")
public class KecamatanController {
	private final KecamatanRepository kecRep;

	@Autowired
	public KecamatanController(KecamatanRepository kecRep) {
		this.kecRep = kecRep;
	}

	// http://localhost:1919/Kecamatan
	@GetMapping
	public List<KecamatanDto> get() {
		List<KecamatanEntity> kecamatanList = kecRep.findAll();
		List<KecamatanDto> kecamatanDtoList = kecamatanList.stream().map(this::convertToDto)
				.collect(Collectors.toList());
		return kecamatanDtoList;
	}

	@GetMapping("/{code}")
	public KecamatanDto get(@PathVariable String code) {
		if (kecRep.findById(code).isPresent()) {
			KecamatanDto kecamatanDto = convertToDto(kecRep.findById(code).get());
			return kecamatanDto;
		}
		return null;
	}

	/* Insert Data */
	@PostMapping
	public KecamatanDto insert(@RequestBody KecamatanDto dto) {
		KecamatanEntity kecamatan = convertToEntity(dto);
		kecRep.save(kecamatan);
		return dto;
	}

	private KecamatanEntity convertToEntity(KecamatanDto dto) {
		KecamatanEntity kecamatan = new KecamatanEntity();
		kecamatan.setKodeKecamatan(dto.getCode());
		kecamatan.setNamaKecamatan(dto.getKecamatan());
		return kecamatan;
	}

	private KecamatanDto convertToDto(KecamatanEntity kecamatan) {
		KecamatanDto kecamatanDto = new KecamatanDto();
		kecamatanDto.setCode(kecamatan.getKodeKecamatan());
		kecamatanDto.setKecamatan(kecamatan.getNamaKecamatan());
		return kecamatanDto;
	}
}