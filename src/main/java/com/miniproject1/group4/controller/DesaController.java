
package com.miniproject1.group4.controller;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.miniproject1.group4.dto.DesaDto;
import com.miniproject1.group4.entity.DesaEntity;
import com.miniproject1.group4.repository.DesaRepository;

@RestController
@RequestMapping("/desa")
public class DesaController {
	private final DesaRepository desaRepository;

	@Autowired
	public DesaController(DesaRepository desaRepository) {
		this.desaRepository = desaRepository;
	}

	@GetMapping
	public List<DesaDto> get() {
		List<DesaEntity> dlist = desaRepository.findAll();
		List<DesaDto> dDtoList = dlist.stream().map(this::convertToDto).collect(Collectors.toList());
		return dDtoList;
	}

	@GetMapping("/{code}")
	public DesaDto getList(@PathVariable String code) {
		if (desaRepository.findById(code).isPresent()) {
			DesaDto desaDto = convertToDto(desaRepository.findById(code).get());
			return desaDto;
		}
		return null;

	}

	@PostMapping
	public DesaDto insert(@RequestBody DesaDto dto) {
		DesaEntity desa = convertToEntity(dto);
		desaRepository.save(desa);
		return dto;
	}

	private DesaEntity convertToEntity(DesaDto dto) {
		DesaEntity desa = new DesaEntity();
		desa.setKodeDesa(dto.getCodeDesa());
		desa.setNamaDesa(dto.getNamaDesa());
		return desa;
	}

	private DesaDto convertToDto(DesaEntity desa) {
		DesaDto desaDto = new DesaDto();
		desaDto.setCodeDesa(desa.getKodeDesa());
		desaDto.setNamaDesa(desa.getNamaDesa());
		return desaDto;
	}
}