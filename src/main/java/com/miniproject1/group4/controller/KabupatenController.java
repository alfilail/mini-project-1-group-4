package com.miniproject1.group4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.miniproject1.group4.dto.KabupatenDto;
import com.miniproject1.group4.entity.KabupatenEntity;
import com.miniproject1.group4.repository.KabupatenRepository;

import java.util.stream.Collectors;
import java.util.List;

@RestController
@RequestMapping("/kabupaten")

public class KabupatenController {
	private final KabupatenRepository kabupatenRepository;

	@Autowired
	public KabupatenController(KabupatenRepository kabupatenRepository) {
		this.kabupatenRepository = kabupatenRepository;
	}

	@GetMapping
	public List<KabupatenDto> get() {
		List<KabupatenEntity> kabupatenList = kabupatenRepository.findAll();
		List<KabupatenDto> kabupatenDtoList = kabupatenList.stream().map(this::convertToDto)
				.collect(Collectors.toList());
		return kabupatenDtoList;

	}

	@GetMapping("/{code}")
	public KabupatenDto get(@PathVariable String code) {
		if (kabupatenRepository.findById(code).isPresent()) {
			KabupatenDto kabupatenDto = convertToDto(kabupatenRepository.findById(code).get());
			return kabupatenDto;
		}
		return null;
	}

	@PostMapping
	public KabupatenDto insert(@RequestBody KabupatenDto dto) {
		KabupatenEntity kabupatenEntity = convertToEntity(dto);
		kabupatenRepository.save(kabupatenEntity);
		return dto;
	}

	private KabupatenEntity convertToEntity(KabupatenDto dto) {
		KabupatenEntity kabupatenEntity = new KabupatenEntity();
		kabupatenEntity.setKodeKabupaten(dto.getCodeKabupaten());
		kabupatenEntity.setNamaKabupaten(dto.getNamaKabupaten());
		return kabupatenEntity;

	}

	private KabupatenDto convertToDto(KabupatenEntity kabupatenEntity) {
		KabupatenDto kabupatenDto = new KabupatenDto();
		kabupatenDto.setCodeKabupaten(kabupatenEntity.getKodeKabupaten());
		kabupatenDto.setNamaKabupaten(kabupatenEntity.getNamaKabupaten());
		return kabupatenDto;
	}
}
