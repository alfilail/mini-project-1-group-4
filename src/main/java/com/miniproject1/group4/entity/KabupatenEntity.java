package com.miniproject1.group4.entity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_kabupaten")
public class KabupatenEntity {
	@Id
	@Column(name = "kodekab", length = 25)
	private String kodeKabupaten;
	
	@Column(name = "namakab")
	private String namaKabupaten;
	
	public String getKodeKabupaten() {
		return kodeKabupaten;
	}

	public void setKodeKabupaten(String kodeKabupaten) {
		this.kodeKabupaten = kodeKabupaten;
	}
	
	public String getNamaKabupaten() {
		return namaKabupaten;
	}
	
	public void setNamaKabupaten(String namaKabupaten) {
		this.namaKabupaten = namaKabupaten;
	}
}
