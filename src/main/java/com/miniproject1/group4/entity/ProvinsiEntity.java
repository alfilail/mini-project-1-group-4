package com.miniproject1.group4.entity;

import javax.persistence.*;

@Entity
@Table(name = "provinsi")
public class ProvinsiEntity {
	@Id
	@Column(name = "kodeprov", length = 25)
	private String kodeProvinsi;
	
	@Column(name = "namaprov")
	private String namaProvinsi;

	public String getKodeProvinsi() {
		return kodeProvinsi;
	}

	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}

	public String getNamaProvinsi() {
		return namaProvinsi;
	}

	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
}
