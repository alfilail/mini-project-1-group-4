package com.miniproject1.group4.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_desa")
public class DesaEntity {

	@Id
	@Column(name = "kddesa", length = 25)
	private String kodeDesa;

	@Column(name = "nmdesa")
	private String namaDesa;

	@ManyToOne
	@JoinColumn(name = "kdprov", nullable = false)
	private ProvinsiEntity provinsi;

	@ManyToOne
	@JoinColumn(name = "kdkab", nullable = false)
	private KabupatenEntity kabupaten;

	@ManyToOne
	@JoinColumn(name = "kdkec", nullable = false)
	private KecamatanEntity kecamatan;

	public String getKodeDesa() {
		return kodeDesa;
	}

	public void setKodeDesa(String kodeDesa) {
		this.kodeDesa = kodeDesa;
	}

	public String getNamaDesa() {
		return namaDesa;
	}

	public void setNamaDesa(String namaDesa) {
		this.namaDesa = namaDesa;
	}

}
